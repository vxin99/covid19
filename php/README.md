createTables.php creates two tables "countries" and "covid19". The first table will have the strucuture id, and name, the second table will have the structure id, date, countryId, newConfirmed, newDeath
Both tables will be uploaded to SQL

In insert.php, first the data already inside of the SQL table will be truncated. 
Reading from a file, the names of countries will be mapped to an associative array and put into the first SQL table "countries"
After reading the first line and mapping country names, we start reading data from the file. Data is placed into the second table "covid19" according to their countryId

graph.php takes the names of the top 5 countries from the SQL first table "countries" and graphs 5 lines with the 7 day average on a PHP graph included with a legend

index.php takes data from the last date of the SQL table and creates a table in html of the top 5 new confirmed cases countries, showing their name, new confirmed cases, and new deaths
included below the table is the graph created by graph.php