<?php
$con = mysqli_connect($db_host,$db_user,$db_pass,$db_name, 3306);
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}
require_once 'phplot.php';

// read countries
$countries = array();
$sql1 = "select id, name from countries";
$country = mysqli_query($con, $sql1);
while($row = $country->fetch_assoc()) {
	//echo $row["id"].' '.$row["name"]."<br>";
	$countries[$row["id"]] = $row["name"];
}
$sql2 = "SELECT * FROM covid19 WHERE id=(SELECT max(id) FROM covid19)";
$query = mysqli_query($con, $sql2);
$lastrow = $query->fetch_assoc();
$lastday = $lastrow["date"];

?>
<!DOCTYPE html>
<html>
<head>
	<title>Top 5 covid19 data</title>
	<style>
	table, th, td {
  	border: 1px solid black;
  	}
</style>
</head>
<body>
	<center>
<?
echo "Confirmed Cases ".$lastday;
?> 
<br>
 <table style="width:80%">
  <tr>
    <th>Country</th>
    <th>NewConfirmed</th>
  </tr>
  <br/>
 <?
 $sql = "select c.name, d.newConfirmed from countries c, covid19 d where d.date='$lastday' and c.id=d.countryId";
 $countrydata = mysqli_query($con, $sql);
while($row = $countrydata->fetch_assoc()) {
	
	$country = $row["name"];
	$newConfirmed = $row["newConfirmed"];
    echo "<tr> <td style='text-align:center'> $country</td><td style='text-align:center'>$newConfirmed</td> </tr>";
}
?>
</table> 
<br>
<hr>
<br>
<img src="graph.php" border=0 align="left">
<img src="covideath.php" border=0 align="right">

</center>
</body>
</html>
