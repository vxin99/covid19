import pandas as pd
import numpy as np
from datetime import date
import re
import requests

#source url
source = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'

def creatfile(path,string):
    with open(path,'w') as f:
        f.write(string)

def request_handle(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36 Edg/79.0.309.71'}
    r = requests.get(url, headers = headers, timeout= 5)
    coding = r.apparent_encoding
    r.encoding = coding
    return r

datestr = date.today()
r = request_handle(source)
filename = source[source.rfind("/")+1:]
path_full = "./"+filename
creatfile(path_full, r.text)

df = pd.read_csv(filename, error_bad_lines=False)

df1 = df.drop(['Province/State','Lat','Long'], 1)

df2 = df1.groupby(['Country/Region']).sum()

# sort table by last column
df3 = df2.sort_values(df2.columns[-1], ascending = False)

df4 = df3.diff(axis=1)
df5 = df4.drop(df4.columns[0], axis = 1)
df6 = df5.transpose()
df61 = df6.iloc[:, : 5]
df61.to_csv("newDeath.csv")
df61

rolling_mean = df6.rolling(7, min_periods=1, axis = 0).mean()
rolling_mean

df7 = rolling_mean.iloc[:, : 5]
df7.to_csv(r'currentDeath.csv')

import pysftp

srv = pysftp.Connection(host="home189948116.1and1-data.host", username="u43521350-vxin",password="Learn2bPro!")
srv.put('newDeath.csv')
srv.put('currentDeath.csv')

# Closes the connection
srv.close()

