Both files read data from https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/
to get total confirmed and total deaths for covid19.
Then they will convert the data into a Pandas database.
The province section is deleted and same country data is grouped together.
After sorting the data and finding the new confirmed or new death by substracting previous date data, the top 5 countries are taken from table and written to a new file
Take the previous data and find the rolling mean for 7 days for top 5 countries and export that data to a file as well.

run.sh is entrypoint of the container and will execute both python scripts and generate csv files before starting the apache web server