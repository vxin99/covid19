import pandas as pd
import numpy as np
from datetime import date
import re
import requests

#source url
source = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'

#create new file
def creatfile(path,string):
    with open(path,'w') as f:
        f.write(string)

def request_handle(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36 Edg/79.0.309.71'}
    r = requests.get(url, headers = headers, timeout= 5)
    coding = r.apparent_encoding
    r.encoding = coding
    return r

#retrieve data from sourc and create new file in current directory
datestr = date.today()
r = request_handle(source)
filename = source[source.rfind("/")+1:]
path_full = "./"+filename
creatfile(path_full, r.text)

#create dataframe off csv data
df = pd.read_csv(filename, error_bad_lines=False)

#remove Province column
df1 = df.drop(['Province/State','Lat','Long'], 1)

#group same country data together and find sum
df2 = df1.groupby(['Country/Region']).sum()
df2

#sort data
df3 = df2.sort_values(df2.columns[-1], ascending = False)

#organize data into top five countries and export to csv
df4 = df3.diff(axis=1)
df5 = df4.drop(df4.columns[0], axis = 1)
df6 = df5.transpose()
df61 = df6.iloc[:, : 5]
df61.to_csv("newConfirmed.csv")

#find rolling mean of top 5 Countries and export to csv
rolling_mean = df6.rolling(7, min_periods=1, axis = 0).mean()

df7 = rolling_mean.iloc[:, : 5]

df7.to_csv(r'currentdata.csv')

