<?php 
require_once 'phplot.php';

$data = array();
$filename = "/work/currentdata.csv";
$myfile = fopen($filename, "r") or die("Unable to open file!");
$first = fgets($myfile);
$countries = explode(",", $first);
array_shift($countries);
while(! feof($myfile)) {
  $line = fgets($myfile);

   // parse numbers out of line
	$c = explode(",", $line);
	$d = explode("/", $c[0]);

	$newDateString = $d[2].'-'.$d[0].'-'.$d[1];

	$row = array($newDateString, $c[1], $c[2], $c[3], $c[4], $c[5]);
	array_push( $data, $row );
}
fclose($myfile);
#label axises and set parameters
$plot = new PHPlot(800, 600);
$plot->SetImageBorderType('plain');

$plot->SetPlotType('lines');
//$plot->SetDataType('data-data');
$plot->SetDataType('text-data');
$plot->SetLegend($countries);
$plot->SetLegendPosition(3, 0, 'title', 0.5, 3);
$plot->SetDataValues($data);
$plot->SetXLabelAngle(90);
# Main plot title:
$plot->SetTitle('Top 5 Countries Confirmed Cases 7 day average');

$plot->DrawGraph();



?>
