Practiced combining Python, PHP, and SQL to display and organize data from a data source.
Used Python to find new confirmed cases and deaths as well as their 7 day average.
Used PHP to display a graph of the 7 day average and a table of new confirmed cases on a certain day.